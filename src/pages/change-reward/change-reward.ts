import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,LoadingController,ActionSheetController } from 'ionic-angular';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { Storage } from '@ionic/storage';
import {FormBuilder,Validators,FormGroup} from '@angular/forms';
import { AlertController} from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';


@Component({
  selector: 'page-change-reward',
  templateUrl: 'change-reward.html',
})
export class ChangeRewardPage {
  data1;checkNeft:boolean;
  bankDetails :any;
  isChallanSubmit : boolean =false;
  bank_page:FormGroup;items;type;id;val;image;rewardType;
  bankList=['axis bank','canara bank','idbi bank'];
  canLogin : boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams ,
    public types : UserserviceProvider,private toastCtrl: ToastController,
    public storage:Storage,public loadingCtrl: LoadingController,
    private _form:FormBuilder,public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,private camera: Camera) {

   
    this.checkNeft=false;
    this.type=false;
    this.bank_page=this._form.group({
        reward_type:['',Validators.compose([Validators.required])],
        bank:[''],
        ifsc:[''],
        acc_name:[''],
        acc_no:[''],
        api_token:[''],
        image:[],
        bank_name:[]
    });

    this.storage.get('can_login').then((val) => {
          
          console.log('login',val);
          if(val==true){
            console.log('true');
            this.canLogin=true;

            this.rewards();
    this.getBankDetials();
            this.loadData();   
    this.initializeItems();
          }
          else{
            console.log('false');
            this.canLogin=false;
          }

        });


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangeRewardPage');
  }

       getBankDetials(){

        this.storage.get('api_token').then((value)=>{
              this.types.getBankData(value).then((val)=>{
                  console.log(val);
                  console.log(val['data'].length);
                  this.items=val;
                 /* for(var i=0;i<val['data'].length;i++){
                    this.items.push(val['data'][i].name);
                  }*/
              this.bankDetails=this.items;
              });
            });
      }

  rewards()
  {
     let loader = this.loadingCtrl.create({
        content: "Please wait..."
    });
    loader.present();
    this.storage.get('api_token').then((val)=>{
        this.types.getRewardTypes(val).then(data2=>{
            loader.dismiss();      

            this.data1=data2['data'];
          }).catch(err=>{
            loader.dismiss();      
            console.log(err);
        });
    });

    
  }

  update() {
    console.log('reward type',this.bank_page.value['reward_type']);
    if(this.bank_page.value['reward_type']=='Neft'){
      if(!this.isChallanSubmit){
     //   this.presentAlert('Please Upload Cheque');
     let alert = this.alertCtrl.create({
      
      subTitle: 'Please Upload Cheque',
      buttons: [
        {
          text: 'Ok',
        }
      ]
    });
    alert.present();
    return;
      }
    }
    
    this.bank_page.controls.bank.setValue(this.id);
    this.storage.get('api_token').then((val)=>{
        this.bank_page.controls.api_token.setValue(val);
        var formdata=this.bank_page.getRawValue();
        if(this.image!=""){ 
          this.bank_page.controls.image.setValue(this.image);
        }
        this.types.changeRewardType(this.bank_page.getRawValue()).then(dataSet=>{
          console.log(dataSet['status']);
          if(dataSet['status']==true)
          {

             this.types.getProfileData(val).then((dataSet)=>{
              console.log(dataSet);              
              this.storage.set('profile',dataSet['data']);

            });
              this.presentAlert(dataSet['message']);


          }
        }).then(err=>{

        })
    });
  } 
  selectReward(e){
    console.log(e);
    console.log('bank',this.bank_page.value['reward_type']);
    this.storage.set('reward_type',this.bank_page.value['reward_type']);

    console.log(this.data1);
    for(var i=0;i<this.data1.length;i++){
      if(this.data1[i].slug==e){
        console.log(this.data1[i].slug);
        var dataReward=this.data1[i]; 

      }
    }
   
    console.log(dataReward);
    if(dataReward.bank_status == 1){
        this.checkNeft=true;
        this.rewardSelect();
        console.log(this.checkNeft);
    }else{
      this.checkNeft = false;
      console.log(this.checkNeft);
    }
    console.log(this.checkNeft);

    if(dataReward.bank_status != 1){
            this.rewardSelect();
    }
          console.log(this.checkNeft);

  }
  loadData(){
    this.storage.get('profile').then((val) => {
      console.log(val);
      if(val.bank_details!='' && val!= undefined && val.bank_details!=undefined ){
          this.bank_page.controls.reward_type.setValue(val.bank_details.reward_type);
          this.bank_page.controls.bank_name.setValue(val.bank_details.bank); 
          this.bank_page.controls.ifsc.setValue(val.bank_details.ifsc);
          this.bank_page.controls.acc_no.setValue(val.bank_details.acc_no);
          this.bank_page.controls.acc_name.setValue(val.bank_details.acc_name);
          if(val.bank_details.bank_status==1){
          	    this.checkNeft=true;
          }
          console.log(val.bank_details.bank);
          this.dataSet(val.bank_details.bank);
      }
    });
  }
  rewardSelect(){
    this.storage.get('profile').then((val) => {
      console.log(val);
      if(val.bank_details!='' && val!= undefined && val.bank_details!=undefined ){
          this.bank_page.controls.bank_name.setValue(val.bank_details.bank); 
          this.bank_page.controls.ifsc.setValue(val.bank_details.ifsc);
          this.bank_page.controls.acc_no.setValue(val.bank_details.acc_no);
          this.bank_page.controls.acc_name.setValue(val.bank_details.acc_name);
          this.bank_page.controls.bank.setValue(''); 

          /*if(val.bank_details.bank_status==1){
                this.checkNeft=true;
          }*/
          console.log(val.bank_details.bank);
          this.dataSet(val.bank_details.bank);
      }
    });
    this.storage.set('reward_type',this.bank_page.value['reward_type']);
    console.log("reward",this.bank_page.value['reward_type']);
  }
  presentAlert(message) {
    let alert = this.alertCtrl.create({
      
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.setRoot(this.navCtrl.getActive().component);
          }
        }
      ]
    });
    alert.present();
  }
   getItems(ev) {
    // Reset items back to all of the itema
           
            var name=ev;
        // if the value is an empty string don't filter the items
            if (name && name.trim() != '') {
              this.type=true;
             // console.log("search",this.items);
              this.bankDetails = this.items.filter((item) => {
                console.log(item);
                return (item.toLowerCase().indexOf(name.toLowerCase()) > -1);
              })
            }
            else{
              this.type=false;
            }
            console.log(this.bankDetails.length);
            if(this.bankDetails.length<=0){
             // this.bank_page.disable.apply;
             // console.log(this.bank_page.disable());
            }
   
  }

    initializeItems() {
      //this.items=[''];
      this.storage.get('api_token').then((value)=>{
        this.types.getBankData(value).then((val)=>{
            console.log(val);
            console.log(val['data'].length);
            this.items=[];
            for(var i=0;i<val['data'].length;i++){
              this.items.push(val['data'][i].name);
            }
            console.log(this.items);
        })
      });
    
  }
  
  dataSet(i){
    console.log(i);
    this.bank_page.controls.bank_name.setValue(i); 
    this.type=false;
     this.storage.get('api_token').then((value)=>{
        this.types.getBankData(value).then((val) =>{
            for(var i=0;i<val['data'].length;i++){
              if(this.bank_page.controls.bank_name.value == val['data'][i].name){
                   this.id=val['data'][i].id;   
                }           
            }
            this.bank_page.controls.bank.setValue(this.id);
            console.log(this.bank_page.controls.bank.value);
        });

      });
  }

  uploadImage(){
    this.presentActionSheet();
  }

        presentActionSheet(){

           let actionSheet = this.actionSheetCtrl.create({
          title: 'Take Photo',
          buttons: [
             {
               text: 'camera',
               handler: () => {
                 console.log('camera clicked');
                 this.takePhoto(1);
               }
             },
             {
               text: 'Gallery',
               handler: () => {
                 console.log('Gallery clicked');
                 this.takePhoto(0);
               }
             }
           ]
           });

        actionSheet.present();
        };
    

    takePhoto(val) {
      const options: CameraOptions = {
        quality: 30,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        correctOrientation: true,
        sourceType:val,
    };

    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      console.log(base64Image);
      this.isChallanSubmit=true;
      this.image=base64Image;

    }, (err) => {
      // Handle error
    });
    
  }
}
