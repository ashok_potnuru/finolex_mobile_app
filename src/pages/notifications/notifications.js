var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { Storage } from '@ionic/storage';
import { VideoPlayer } from '@ionic-native/video-player';
var NotificationsPage = /** @class */ (function () {
    function NotificationsPage(navCtrl, navParams, storage, types, loadingCtrl, videoPlayer) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.types = types;
        this.loadingCtrl = loadingCtrl;
        this.videoPlayer = videoPlayer;
        this.loadData();
    }
    NotificationsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NotificationsPage');
    };
    NotificationsPage.prototype.loadData = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Please wait..."
        });
        loader.present();
        this.storage.get('api_token').then(function (val) {
            loader.dismiss();
            _this.types.getNotificationData(val).then(function (data2) {
                _this.data = data2['data'];
                console.log('lenght', _this.data);
                _this.notifications = _this.data['notifications'];
                console.log('lenght', _this.notifications);
                /*if(this.notifications.lenght<0){
                      // this.historyShow=false;
                }
                else{
                    this.notifications=true;
                }*/
                console.log(_this.data);
            }).catch(function (err) {
                console.log(err);
            });
        });
    };
    NotificationsPage.prototype.openVideo = function (notify) {
        console.log('video', notify.video);
        this.videoPlayer.play(notify.video).then(function () {
            console.log('video completed');
        }).catch(function (err) {
            console.log(err);
        });
    };
    NotificationsPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-notifications',
            templateUrl: 'notifications.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams,
            Storage, UserserviceProvider, LoadingController, VideoPlayer])
    ], NotificationsPage);
    return NotificationsPage;
}());
export { NotificationsPage };
//# sourceMappingURL=notifications.js.map