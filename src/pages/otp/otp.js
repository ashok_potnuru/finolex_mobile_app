var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { CheckotpProvider } from '../../providers/checkotp/checkotp';
import { Storage } from '@ionic/storage';
import { ConfirmpasswordPage } from '../confirmpassword/confirmpassword';
var OtpPage = /** @class */ (function () {
    function OtpPage(navCtrl, navParams, _form, checkotp, loadingController, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._form = _form;
        this.checkotp = checkotp;
        this.loadingController = loadingController;
        this.storage = storage;
        this.mobileNumber = this.navParams.get('mobileNumber');
        this.role = this.navParams.get('role');
        this.form = this._form.group({
            otp: ['', Validators.compose([Validators.required, Validators.maxLength(4), Validators.minLength(4)])]
        });
    }
    OtpPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OtpPage');
    };
    OtpPage.prototype.next = function (el) {
        el.setFocus();
    };
    OtpPage.prototype.sendOtp = function () {
        var _this = this;
        //    this.totalText=this.text1+this.text2+this.text3+this.text4;
        console.log(this.totalText);
        this.loader = this.loadingController.create({
            content: 'Please Wait....',
        });
        this.loader.present();
        this.checkotp.validateOtp(this.mobileNumber, this.totalText)
            .then(function (data) {
            _this.loader.dismiss();
            if (data['status'] == true) {
                _this.navCtrl.push(ConfirmpasswordPage, { mobileNumber: _this.mobileNumber, role: _this.role });
            }
            else {
                alert(data['message']);
            }
            //	this.navCtrl.push(ConfirmpasswordPage);
            console.log(data);
        });
    };
    OtpPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-otp',
            templateUrl: 'otp.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, FormBuilder, CheckotpProvider,
            LoadingController, Storage])
    ], OtpPage);
    return OtpPage;
}());
export { OtpPage };
//# sourceMappingURL=otp.js.map