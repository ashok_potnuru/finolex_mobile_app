import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,ToastController,AlertController } from 'ionic-angular';
import {FormBuilder,Validators,FormGroup} from '@angular/forms';
import {CheckotpProvider} from '../../providers/checkotp/checkotp';
import {TabsScreenPage} from '../tabs-screen/tabs-screen';
import { Storage } from '@ionic/storage';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import {ConfirmpasswordPage} from '../confirmpassword/confirmpassword';
import {DashboardPage} from '../dashboard/dashboard';
import {ProfilePage} from '../profile/profile';


@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html',
})

export class OtpPage {

form:FormGroup;
text1:any;
text2:any;
text3:any;
text4:any;
totalText: any;
mobileNumber : any;
loader : any;role;
data;
token;
  profileData;
  register_form:FormGroup;
  segment;
  isDealer : boolean =false;
  deviceToken;
  roleType;
  mismatchedPasswords : boolean = false;
  gstDocs = "";


  constructor(public navCtrl: NavController, public navParams: NavParams,private _form: FormBuilder,public checkotp : CheckotpProvider,
  	public loadingController : LoadingController,private storage: Storage,
  	public user: UserserviceProvider,
          private toastCtrl: ToastController,public alert: AlertController) {

  	this.data=this.navParams.get('data');

    console.log('data',this.data);

  //	this.mobileNumber=this.navParams.get('mobileNumber');
 // 	console.log('mobile',this.mobileNumber)
  	console.log('data',this.data.mobile_no);
    console.log('page',this.data.page); 

  	this.form=this._form.group({
        otp:['',Validators.compose([Validators.required,Validators.maxLength(4),Validators.minLength(4)])]
    });

    if(this.data!=undefined || this.data!=null){
    	console.log('fdfdfdf')
    	this.mobileNumber=this.data.mobile_no;
    }
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtpPage');
  }

  next(el) {
    el.setFocus();
  }
  sendOtp(){
//    this.totalText=this.text1+this.text2+this.text3+this.text4;
	  console.log(this.totalText);

	  this.loader= this.loadingController.create({
						      content:'Please Wait....',
						    });
						    this.loader.present();
						  //  alert(this.data.mobile_no);
						    //alert(this.totalText)

               // alert(this.data.page)

                this.mobileNumber=this.data.mobile_no;

	  this.checkotp.validateOtp(this.mobileNumber,this.totalText)
	  .then(data => {
			    	this.loader.dismiss();
			    	if(data['status']==true){
              var page=this.data.page; 
              if(page=="TabsScreenPage"){
                this.register();
              }
              else if(page=="ChangePasswordPage"){
                this.changePassword();

              }
              else if(page=="ForgotPasswordPage"){

                console.log('no',this.mobileNumber);
                this.navCtrl.push(ConfirmpasswordPage,{mobileNumber:this.mobileNumber});
              }
              else if(page=="ProfilePage"){
                this.updateProfile();
              }


     		/*if(this.data==undefined || this.data==null){

	 				this.navCtrl.push(ConfirmpasswordPage,{mobileNumber:this.mobileNumber});
	 			}
	 			else{
	 				this.changePassword();

	 			}
			  */  	}
			    	else{
			    	//	alert(data['message']);
			    	}
			    //	this.navCtrl.push(ConfirmpasswordPage);
		    	console.log(data);
			    });

  }

  register(){

    this.user.register(this.data.details,this.data.gstDocs).then((dataSet)=>{

        if(dataSet['status']== true){  
          this.storage.clear();
          this.storage.set('api_token',dataSet['data'].api_token);
          this.storage.set('image',dataSet['data'].image);
          this.storage.set('mobile_no',dataSet['data'].mobile);
          this.storage.set('user_name',dataSet['data'].name);  
          this.storage.set('role',dataSet['data'].role);
          this.storage.get('api_token').then((val) => {
        
          this.token=val;
            this.user.getProfileData(dataSet['data'].api_token).then((dataSeta)=>{
              this.profileData=dataSeta['data'];
              this.storage.set('profile',dataSeta['data']);
            });
         });
           // this.navCtrl.setRoot(DashboardPage);
          this.presentAlert(dataSet['message']);
          this.navCtrl.setRoot(TabsScreenPage,{name:'LogIn'});
        }else{
          this.presentAlert(dataSet['message']);
          this.navCtrl.setRoot(TabsScreenPage,{name:'Register'});
        }
       
     });
  }

  updateProfile(){

    this.user.updateProfile(this.data.details).then(dataSet=>{
      console.log(dataSet);
      console.log(dataSet['status']);
      if(dataSet['status'] == true){
        this.storage.set('profile',dataSet['data']).then(data=>{
          console.log('profile_data',data);

          let toast = this.toastCtrl.create({
                  message: dataSet['message'],
                  duration: 3000,
                  position: 'bottom'
                });

                toast.present();
                this.navCtrl.push(ProfilePage);
         // this.updateDataOnPage();          
         // this.isReadonly=true;  
         // console.log(this.isReadonly);          
        });
        console.log(dataSet['data']['profile_details']['name']);
       // this.profile_page.controls.name.setValue(dataSet['data']['profile_details']['name']);
      }
    });

  }
  changePassword(){
  	this.storage.get('api_token').then((val)=>{

  		this.data['api_token']=val;
  		console.log(this.data)

  		//this.data.push('api_token',val)

  		this.user.changePassword(this.data).then(dataset=>{
  			 console.log(dataset);
         console.log(dataset['status']);
       //  this.presentAlert(dataset['message']);
        var message=dataset['message'];

         if(dataset['status']==true){

              let alert = this.alert.create({
                title: 'Finolex',
                message: message,
                buttons: [
                  {
                    text: 'Ok',
                    role: 'cancel',
                    handler: () => {
                      console.log('Cancel clicked');
                       this.navCtrl.setRoot(DashboardPage);
                    }
                  }
                 ]              
              });
              
              alert.present();            
         }else{
          
         }
         let toast = this.toastCtrl.create({
              message: dataset['message'],
              duration: 3000,
              position: 'top'
            });
         toast.present();

  		});
  });

  }

  presentAlert(message) {
    let alert = this.alert.create({
      
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.setRoot(this.navCtrl.getActive().component);
            console.log('Ok clicked');
          }
        }
      ]
    });
    alert.present();
  }

}
