import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,LoadingController } from 'ionic-angular';
import {FormBuilder,Validators,FormGroup} from '@angular/forms';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { Storage } from '@ionic/storage';
import { ActionSheetController } from 'ionic-angular'
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AlertController,ModalController} from 'ionic-angular';
import {ModalpagePage} from '../modalpage/modalpage';
import { DashboardPage } from '../dashboard/dashboard';
import { Events } from 'ionic-angular';
import {OtpPage} from '../otp/otp';
import {ForgotpasswordProvider} from '../../providers/forgotpassword/forgotpassword';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
}) 	
export class ProfilePage {

  profile_page:FormGroup;
  token;
  loader : any;
  language : any;
  isEditable : boolean =true;
  reward_type : String="Not Yet Selected";
  isEnglish : boolean = true;
  isHindi : boolean = false;
  isValidPinode : boolean = true;
  profileData;isSubmit:boolean;
  image;name;isReadonly:boolean;isReadPic:boolean;
  data : any;
  canLogin : boolean =true;
 // defaultImage="https://images.pexels.com/photos/248797/pexels-photo-248797.jpeg?auto=compress&cs=tinysrgb&h=350";

  constructor(public navCtrl: NavController, public navParams: NavParams,
  			  private _form:FormBuilder,public user:UserserviceProvider,
  			  private storage: Storage,public actionSheetCtrl: ActionSheetController,
  			  private camera: Camera,public alert: AlertController,
  			  public loadingController : LoadingController,
  			  private toastCtrl: ToastController,public modalCtrl : ModalController,
  			  public events: Events,
  			  private forgotProvider : ForgotpasswordProvider) {
  	this.profile_page=this._form.group({
	  		name:[''],
	  		gender:[''],
	  		date_of_birth:[''],
	  		language:[''],
	  		address:['',Validators.compose([Validators.required])],
	  		city:['',Validators.compose([Validators.required])],
	  		state:['',Validators.compose([Validators.required])],
	  		pincode:['',Validators.compose([Validators.required,Validators.maxLength(6),Validators.minLength(6)])],	
	  		api_token:['']
	  	});

  	this.storage.get('can_login').then((val) => {
          
          console.log('login',val);
          if(val==true){
            this.canLogin=true;
          }
          else{

            this.canLogin=false;
          }

        });

  	this.updateDataOnPage();
  	
	this.storage.get('api_token').then((val)=>{
		this.profile_page.controls.api_token.setValue(val);
	})

	this.storage.get("role").then((val)=>{
		console.log("role",val);
		if(val=="dealer"){
			this.isEditable=false;
		}
		else{
			this.isEditable=true;
		}
	});

	this.storage.get("language").then((val)=>{
		console.log("lan",val);
		if(val=="Hindi"){
			this.isHindi=true;
			this.isEnglish=false;
		}
		else if(val=="English"){
		this.isHindi=false;
			this.isEnglish=true;	
		}

	});

	this.isReadonly=true;
	this.isReadPic=true;
	this.isSubmit=false;

	/*this.storage.get('reward_type').then((val)=>{
		console.log('reward',val);
		if(val!=null)
		this.reward_type=val;
	})*/
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

	edit(){
		console.log(this.isReadonly)
		if(this.isReadonly == true) {
		this.isReadonly= false;
			console.log(this.isReadonly);
		}
		else{
		this.isReadonly=true;
			console.log(this.isReadonly);
		}
		this.isSubmit=!this.isReadonly;

	}
	editImage(){
		console.log(this.isReadPic)
		if(this.isReadPic==true) {
			console.log(this.isReadPic);

		this.isReadPic=false;
		}
		else{
			console.log(this.isReadPic);

		this.isReadPic=true;
		}
	}
	  	presentActionSheet(){
	   		let actionSheet = this.actionSheetCtrl.create({
		    title: 'TAKE PHOTO',
		    buttons: [
		       {
		         text: 'Camera',
		         handler: () => {
		           console.log('camera clicked');
		           this.takePhoto(1);
		         }
		       },
		       {
		         text: 'Gallery',
		         handler: () => {
		           console.log('Gallery clicked');
		           this.takePhoto(0);
		         }
		       }
	     	]
	   		});

	    actionSheet.present();
    	};
    

  	takePhoto(val) {
	    const options: CameraOptions = {
	      quality: 30,
	      destinationType: this.camera.DestinationType.DATA_URL,
	      encodingType: this.camera.EncodingType.JPEG,
	      mediaType: this.camera.MediaType.PICTURE,
	      correctOrientation: true,
	      sourceType:val,
		};

    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      console.log(base64Image);
      this.updateProfileImage(base64Image);

    }, (err) => {
      // Handle error
    });
  	
	}

	selectLanguage(language){
		console.log('language',language);
		this.language=language;
		/*if(language=="Hindi"){
			this.isHindi=false;
			this.isEnglish=true;
		}
		else if(language=="English"){
			this.isHindi=true;
			this.isEnglish=false;
		}*/	
		this.storage.get('api_token').then((val)=>{

				let data={
			      api_token:val,
			      language:language
			  }

			  this.loader= this.loadingController.create({
						      content:'Please Wait....',
						    });
						    this.loader.present();

			 this.user.changeLanguage(data).then((dataSet)=>{

			 	console.log('languagApi',dataSet);
			 	if(dataSet['status']==true){
			 		this.loader.dismiss();
			 		this.presentAlert(dataSet['message']);
			 		console.log('message',dataSet['message']);

	 		this.storage.set('language',language).then((val)=>{
			this.events.publish('user:language', language);
		});

			 	}
			 });
 		});
	
	}

	validPincode(ev) {
    // Revar 
    var name:string;
    name = ev;

       if(name.length == 6){
       	this.isValidPinode =true;
       }
       else{
       	this.isValidPinode=false;
       }
       console.log(this.isValidPinode);
   
  }


	updateProfileImage(img){
		this.storage.get('api_token').then((val)=>{

			this.user.updateImg(val,img).then((dataSet)=>{

				if(dataSet['status']== true){
				console.log(dataSet);				

		        var imageData=dataSet['data'];
				this.image=imageData['image'];
				console.log(this.image);
				this.profileDetail();
			    this.presentAlert(dataSet['message']);

				this.storage.set('image',imageData['image'])
        		//this.navCtrl.setRoot(DashboardPage);				

				}
				else{
					/* let toast = this.toastCtrl.create({
		              message: dataSet['message'],
		              duration: 3000,
		              position: 'top'
		            });

		            toast.present();*/
		            this.presentAlert(dataSet['message']);


				}

			});	
		});
		this.isReadPic=true;
		this.navCtrl.setRoot(this.navCtrl.getActive().component);
	
	}
	editProfile(){

		console.log(this.isReadonly);

			this.profile_page.controls.language.setValue(this.language);
			console.log('profile',this.profile_page.getRawValue());
          
          this.storage.get('mobile_no').then((val)=>{

          	this.data={
              page:"ProfilePage",
              mobile_no : val,
              details:this.profile_page.getRawValue()
          }

          this.forgotProvider.getForgotPassword(val).then(data => {

         // this.loader.dismiss();

          if(data['status']==true){

      	console.log('otp',this.data);
           
           this.navCtrl.push(OtpPage,{data:this.data});
          }
          else{
          let alertCtrl = this.alert.create({
              title: 'Finolex',
              subTitle: data['message'],
              buttons: ['OK']
            });
            alertCtrl.present();          
          }
            console.log(data);
        });

       });


           //this.navCtrl.push(OtpPage,{data:this.data});

		/*	this.user.updateProfile(this.profile_page.getRawValue()).then(dataSet=>{
			console.log(dataSet);
			console.log(dataSet['status']);
			if(dataSet['status'] == true){
				this.storage.set('profile',dataSet['data']).then(data=>{
					console.log('profile_data',data);

					let toast = this.toastCtrl.create({
		              message: dataSet['message'],
		              duration: 3000,
		              position: 'bottom'
		            });

		            toast.present();
					this.updateDataOnPage();					
					this.isReadonly=true;	
					console.log(this.isReadonly);					
				});*/
			//	console.log(dataSet['data']['profile_details']['name']);
			//	this.profile_page.controls.name.setValue(dataSet['data']['profile_details']['name']);
		//	}
		//});
	}


	profileDetail(){
    this.storage.get('api_token').then((val)=>{

	 this.user.getProfileData(val).then((dataSet)=>{
          console.log('profile_data',dataSet);
          this.profileData=dataSet['data'];
          this.storage.set('profile',dataSet['data']).then(data=>{
          		this.storage.get('profile').then((val) => {
					console.log(val);
				    this.storage.set('user_name',val.profile_details.name);  

				    if(val.profile_details.reward_type!=null || val.profile_details.reward_type!=undefined){

				    this.reward_type=val.profile_details.reward_type;	
				    }

					
					console.log('reward......',val.profile_details.reward_type);

			  		this.profile_page.controls.name.setValue(val.profile_details.name);
			  		this.profile_page.controls.gender.setValue(val.profile_details.gender);
			  		this.profile_page.controls.date_of_birth.setValue(val.profile_details.date_of_birth);
			  		this.profile_page.controls.address.setValue(val.profile_details.address);
			  		this.profile_page.controls.city.setValue(val.profile_details.city);
			  		this.profile_page.controls.state.setValue(val.profile_details.state);
			  		this.profile_page.controls.pincode.setValue(val.profile_details.pincode);

			  		this.image=val.profile_details.image;
			  		if(this.image==null){
			  			this.image="https://images.pexels.com/photos/39811/pexels-photo-39811.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940";
			  		}
			  		this.name=val.profile_details.name;
				});
          });
          console.log(this.profileData);
        });
	});
		
	}
	updateDataOnPage(){
		this.storage.get('profile').then((val) => {
					console.log('profile',val);
					if(val!=null){

					this.storage.set('user_name',val.profile_details.name);  

					this.reward_type=val.profile_details.reward_type;
					console.log('reward...',val.profile_details.reward_type)

			  		this.profile_page.controls.name.setValue(val.profile_details.name);
			  		this.profile_page.controls.gender.setValue(val.profile_details.gender);
			  		this.profile_page.controls.date_of_birth.setValue(val.profile_details.date_of_birth);
			  		this.profile_page.controls.address.setValue(val.profile_details.address);
			  		this.profile_page.controls.city.setValue(val.profile_details.city);
			  		this.profile_page.controls.state.setValue(val.profile_details.state);
			  		this.profile_page.controls.pincode.setValue(val.profile_details.pincode);  	
			  		this.image=val.profile_details.image;
			  		if(this.image==null){
			  			
			  			this.image="assets/images/Blank_Avatar.png";
			  		}
			  		this.name=val.profile_details.name;
		            this.events.publish('user:created', 'user', Date.now());
		        }
		        
				});

	}

	presentAlert(message) {
    let alert = this.alert.create({
      
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.setRoot(this.navCtrl.getActive().component);
            console.log('Ok clicked');
          }
        }
      ]
    });
    alert.present();
  }    
}
