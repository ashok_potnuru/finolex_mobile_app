import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ReedemapiProvider} from '../../providers/reedemapi/reedemapi';
import { LoadingController } from 'ionic-angular';
import { ModalpagePage} from '../modalpage/modalpage';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';



@Component({
  selector: 'page-redeem-history',
  templateUrl: 'redeem-history.html',
})
export class RedeemHistoryPage {

	from_date;
	offset = 0;
	limit = 20;
	totalCount;
	load : any;
	loadedItems : any;
	items=[];
	modalData : any;
  records; historyShow:boolean;
  filterFlag;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public alertCtrl: AlertController,public reedemapi : ReedemapiProvider,
              public loadingCtrl: LoadingController,public modalCtrl : ModalController,
              public storage:Storage,public datepipe: DatePipe) {

  	this.setLoader();
  	this.getReedemHistory();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RedeemHistoryPage');
    console.log(document.getElementById('from_date'));
  }

   setLoader(){
  	 this.load= this.loadingCtrl.create({
      content:'Please Wait....',
    });
    this.load.present();
  }


getReedemHistory(){

   var date=new Date();
   var fromdate=this.datepipe.transform(date, 'yyyy-MM-dd');
   var todate=this.datepipe.transform(date, 'yyyy-MM-dd');
    if(this.modalData!= undefined){
          this.filterFlag=this.filterFlag+1;
         fromdate=this.modalData['fromDate'];
         todate=this.modalData['toDate'];
         this.offset=0;
         if(fromdate == undefined || todate == undefined){
            fromdate='';
            todate='';
            this.filterFlag=0;
            this.offset=0;
         }
    }else{
      this.filterFlag=0;
      fromdate='';
      todate='';
    }
  this.storage.get('api_token').then(token=>{
    console.log('abcd');
      this.items=[];
      console.log("abcd2");
    	this.reedemapi.getReedemHistory(token,this.offset,fromdate,todate)
        .then(dataSet => {
        		this.load.dismiss();
        		console.log(dataSet);
        		let resp = dataSet;
            if(resp['total_count']>0){
              this.historyShow=true;
        		if(resp['status'] == true){
        			console.log(resp['data']);
              this.offset=this.offset+20;
              var dataArray=resp['data'];
              for(var i=0;i<dataArray.length;i++){
                this.items.push(dataArray[i]);
              }
           //   this.items[i].disabled();
            //  this.items[i].enable();
              this.totalCount=resp['total_count'];
              console.log(this.items);

        		}  
            }else{
              this.historyShow=false;
            }      	
     },error=>{
          console.log(error);
     });
  });
}

  pushingList(e){
    var date=new Date();
    var fromdate=this.datepipe.transform(date, 'yyyy-MM-dd');
    var todate=this.datepipe.transform(date, 'yyyy-MM-dd');
    if(this.modalData!= undefined){
         fromdate=this.modalData.fromDate;
         todate=this.modalData.toDate;
    }else{
      fromdate='';
      todate='';
    }
    console.log("in pushing");
    this.storage.get('api_token').then(token=>{

        this.reedemapi.getReedemHistory(token,this.offset,fromdate,todate)
          .then(dataSet => {
              this.load.dismiss();
              console.log(dataSet);
              let resp = dataSet;
              if(resp['status'] == true){
                console.log(resp['data']);
                console.log(this.totalCount);
                if(this.totalCount>this.offset){
                  this.offset=this.offset+20;
                  var dataArray=resp['data'];
                  console.log("array",dataArray.lenght);
                  for(var i=0;i<dataArray.length;i++){
                    console.log("reedem_list",dataArray[i]);
                  this.items.push(dataArray[i]);
              }
             }
              e.complete();
              console.log(this.items);

            }         
     },error=>{
          console.log(error);
          e.complete();
     });
  });
  }

 presentAlert(){

 /*let alert = this.alertCtrl.create({
    title: 'Dates',
    subTitle: '10% of battery remaining',
    inputs: [
        {
          name: 'from Date',
          placeholder: 'From Date',
          type: 'date',
          id: 'from_date'
        },
        {
        name: 'To date',
          placeholder: 'To Date',
          type: 'date'	
        }
      ],
    buttons: ['Dismiss']
  });
  alert.present();*/

  let modal = this.modalCtrl.create(ModalpagePage,{page:RedeemHistoryPage});
    modal.present();

    modal.onDidDismiss(data => {
    	this.modalData=data;
      if(this.modalData != undefined){
        if(this.modalData.fromDate!=''&&this.modalData.toDate!=''){
        	console.log(this.modalData.fromDate);
        	console.log(this.modalData.toDate);
          console.log(data);
          console.log(this.modalData['fromDate']);
          console.log(this.modalData['toDate']);
        }
    }
    this.getReedemHistory();
});
  }

  doInfinite(e) {

    this.pushingList(e);
  }


}
