import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,ActionSheetController } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { WelcomePage } from '../welcome/welcome';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { Storage } from '@ionic/storage';
import {FormBuilder,Validators,FormGroup} from '@angular/forms';
import {ForgotPasswordPage} from '../forgot-password/forgot-password';
import { Firebase  } from '@ionic-native/firebase';
import { AlertController,LoadingController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { TranslateService } from '@ngx-translate/core';
import { Keyboard } from '@ionic-native/keyboard';
import {OtpPage} from '../otp/otp';
import {ForgotpasswordProvider} from '../../providers/forgotpassword/forgotpassword';

@Component({
  selector: 'page-tabs-screen',
  templateUrl: 'tabs-screen.html',
})
export class TabsScreenPage {
  // viewChild for focusing cursor to email input after selecting role
  @ViewChild('Comment') myInput ;

  // settingFirstTimeSlide
  loadSliderVideos;

  loginSection:any;
  registerSection:any;
  form:FormGroup;
  token;
  image;
  role:any;
  profileData;
  register_form:FormGroup;
  segment;
  isDealer : boolean =false;
  deviceToken;
  roleType;
  mismatchedPasswords : boolean = false;
  gstDocs = "";
  checkStatus : boolean =false;
  data : any;
  loader : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
              private userServices: UserserviceProvider,private toastCtrl: ToastController,
              private storage: Storage,
              private camera: Camera,
              public events: Events,
              public alert : AlertController,
              private loaderController : LoadingController,
              private forgotProvider : ForgotpasswordProvider,
             public translateService: TranslateService,
              public actionSheetCtrl: ActionSheetController,
              private _form: FormBuilder,private firebaseIon: Firebase,
              private alertCtrl: AlertController,
              private keyboard: Keyboard) {
  	if(this.navParams.get('name')!=undefined){
  		this.segment=this.navParams.get('name');
  	}else{
  		this.segment="LogIn";
  	}
  
    this.form=this._form.group({
      //  role:['',Validators.compose([Validators.required])],
        mobile_no:['',Validators.compose([Validators.required,Validators.maxLength(10),Validators.minLength(10)])],
        password:['',Validators.compose([Validators.required])],
    });
    this.register_form=this._form.group({
        role:['',Validators.compose([Validators.required])],
        email_id:[''],
        mobile_no:['',Validators.compose([Validators.required,Validators.maxLength(10),Validators.minLength(10)])],
        password:['',Validators.compose([Validators.required])],
        confirm_password:['',Validators.compose([Validators.required])],
        device_token:[],
//        photo:['',Validators.compose([Validators.required])],
      },
      {Validators: this.matchingPasswords('password', 'confirm_password')});
}

    matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    // TODO maybe use this https://github.com/yuyang041060120/ng2-validation#notequalto-1
    return (group: FormGroup): {[key: string]: any} => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      if (password.value != confirmPassword.value) {
          this.mismatchedPasswords= true;
        }
        else{
         this.mismatchedPasswords=false; 
        }
       return this.matchingPasswords;
        };
      }

  ionViewDidLoad() {
  }

  signinSubmit() {  
  
    this.storage.get('deviceToken').then(deviceId=>{
    this.deviceToken=deviceId;
    this.loader= this.loaderController.create({
            content:'Please Wait....',
          });

    this.loader.present();

    this.userServices.login(this.form.getRawValue(),this.deviceToken).then(dataSeta=>{

      this.loader.dismiss();

      if(dataSeta['status']== true){
          this.storage.clear();
        console.log(dataSeta);
          console.log(dataSeta['data'].api_token);
          this.storage.set('api_token',dataSeta['data'].api_token);
          this.storage.set('image',dataSeta['data'].image);
          this.storage.set('mobile_no',dataSeta['data'].mobile_no);
          this.storage.set('role',dataSeta['data'].role);
          console.log('role',dataSeta['data'].role);
          this.storage.set('user_name',dataSeta['data'].name);
          this.storage.set('language',dataSeta['data'].language);
          this.storage.set('can_login',dataSeta['data'].can_login);

          console.log('language',dataSeta['data'].language);
          this.events.publish('user:language', dataSeta['data'].language);
          this.events.publish('user:loggedIn',dataSeta['data'].can_login);


          this.storage.get('api_token').then((val) => {
            this.token=val;
            this.userServices.getProfileData(dataSeta['data'].api_token).then((dataSet)=>{
              console.log(dataSet);
              this.profileData=dataSet['data'];
              this.storage.set('profile',dataSet['data']);
              console.log(this.profileData);
            });
         });

        // tutorial
        this.storage.get('tutorialShown').then((result) => {
            if (!result) {
                this.storage.set('tutorialShown', true);
                this.navCtrl.setRoot(WelcomePage);
            } else {
                this.navCtrl.setRoot(DashboardPage);
            }
        });
      }
      else{
          this.navCtrl.setRoot(this.navCtrl.getActive().component);
          this.presentAlert(dataSeta['message']);
      }
    });
  
  });

  }

  change(){

    console.log('hindi')
    this.translateService.use('hi');

  }

  checkRole(role){
    window.setTimeout(() => {
      this.myInput.setFocus();
    }, 600);
    //this.keyboard.show();
    this.roleType = role.value;
    if(this.roleType=="dealer"){
      this.isDealer=true;
    }
    else{
     this.isDealer=false; 
    }
  }

  openCamera(){
    this.presentActionSheet();
  }

presentActionSheet(){
         let actionSheet = this.actionSheetCtrl.create({
        title: 'TAKE PHOTO',
        buttons: [
           {
             text: 'Camera',
             handler: () => {
               console.log('camera clicked');
               this.takePhoto(1);
             }
           },
           {
             text: 'Gallery',
             handler: () => {
               console.log('Gallery clicked');
               this.takePhoto(0);
             }
           }
         ]
         });

      actionSheet.present();
      };

    takePhoto(val) {
      const options: CameraOptions = {
        quality: 30,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        correctOrientation: true,
        sourceType:val,
    };

    this.camera.getPicture(options).then((imageData) => {
      this.image = 'data:image/jpeg;base64,' + imageData;
      this.gstDocs = this.image;
      this.checkStatus=true;
    }, (err) => {
      // Handle error
    });
    
  }

  signup(){

    if(this.roleType=="dealer"){
      if(this.gstDocs==""){
        this.presentAlert('Please Upload Cancelled Cheque/Gst');
      return;  
      }
    }

      this.storage.get('deviceToken').then(deviceId=>{
      this.register_form.controls.device_token.setValue(deviceId);
      console.log('mobile',this.register_form.controls.mobile_no.value);

       this.loader= this.loaderController.create({
            content:'Please Wait....',
          });
       this.loader.present();

      this.forgotProvider.getOtp(this.register_form.controls.mobile_no.value,this.register_form.controls.role.value)
        .then(data => {

          this.loader.dismiss();

          if(data['status']==true){

            this.data={
              page:"TabsScreenPage",
              mobile_no : this.register_form.controls.mobile_no.value,
              details:this.register_form.getRawValue(),
              gstDocs:this.gstDocs
      }

      console.log('registration',this.data);
           
           this.navCtrl.push(OtpPage,{data:this.data});
          }
          else{
          let alertCtrl = this.alert.create({
              title: 'Finolex',
              subTitle: data['message'],
              buttons: ['OK']
            });
            alertCtrl.present();          
          }
            console.log(data);
        });

//       this.navCtrl.push(OtpPage,{data:this.data});


      /*this.userServices.register(this.register_form.getRawValue(),this.gstDocs).then((dataSet)=>{

        if(dataSet['status']== true){  
          this.storage.clear();
          this.storage.set('api_token',dataSet['data'].api_token);
          this.storage.set('image',dataSet['data'].image);
          this.storage.set('mobile_no',dataSet['data'].mobile);
          this.storage.set('user_name',dataSet['data'].name);  
          this.storage.set('role',dataSet['data'].role);
          this.storage.get('api_token').then((val) => {
        
          this.token=val;
            this.userServices.getProfileData(dataSet['data'].api_token).then((dataSeta)=>{
              this.profileData=dataSeta['data'];
              this.storage.set('profile',dataSeta['data']);
            });
         });
           // this.navCtrl.setRoot(DashboardPage);
          this.presentAlert(dataSet['message']);
          this.navCtrl.setRoot(TabsScreenPage,{name:'LogIn'});
        }else{
          this.presentAlert(dataSet['message']);
        	this.navCtrl.setRoot(TabsScreenPage,{name:'Register'});
        }
       
     }); */

    });
  }

    validatePassword(){
      let password = this.register_form.controls['password'];
      let confirmPassword = this.register_form.controls['confirm_password'];
      if(password.value != confirmPassword.value){
        this.mismatchedPasswords=true;
      }
      else{
        this.mismatchedPasswords=false;
      }
   /*     if(this.newPassword == this.confirmPassword){
          this.isConfirm=true;
          this.checkPass=true;
          console.log(this.isConfirm);
        }/
        else{
          console.log(this.isConfirm);
          this.checkPass=false;
          this.isConfirm=false;
        }*/
        
      }
   verifyNumber(){
      this.navCtrl.push(ForgotPasswordPage);
   }
   presentAlert(message) {
    let alert = this.alertCtrl.create({
      
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok clicked');
          }
        }
      ]
    });
    alert.present();
  }
}