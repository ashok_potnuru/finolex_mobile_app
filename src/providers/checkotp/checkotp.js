var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Http, Headers } from '@angular/http';
var CheckotpProvider = /** @class */ (function () {
    function CheckotpProvider(http) {
        this.http = http;
        this._header = new Headers();
        this._header.append('finolex-user', 'finolex@meltag.io');
        this._header.append('finolex-password', 'finolex$meltag@123');
        this._header.append('Access-Control-Allow-Origin', '*');
        this._header.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    }
    CheckotpProvider.prototype.validateOtp = function (mobileno, otp) {
        var _this = this;
        var data = new FormData();
        data.append("mobile_no", mobileno);
        data.append("otp", otp);
        // let data=JSON.stringify({mobile_no:mobileno,otp:otp});
        return new Promise(function (resolve, reject) {
            _this.http.post("http://app.meltag.com/finolex/api/verify-otp", data, { headers: _this._header }).toPromise().then(function (data) {
                console.log(data);
                resolve(data.json());
            }, function (err) {
                console.log(err);
                reject(err);
            });
        });
    };
    CheckotpProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http])
    ], CheckotpProvider);
    return CheckotpProvider;
}());
export { CheckotpProvider };
//# sourceMappingURL=checkotp.js.map