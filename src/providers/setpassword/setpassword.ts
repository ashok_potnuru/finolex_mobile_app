import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {Http,Headers} from '@angular/http';

@Injectable()
export class SetpasswordProvider {
	_header;

  constructor(public http: Http) {

    console.log('Hello setpassword Provider');
    this._header=new Headers();
    this._header.append('finolex-user','finolex@meltag.io');
    this._header.append('finolex-password','finolex$meltag@123');
    this._header.append('Access-Control-Allow-Origin' , '*');
    this._header.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');

 }
 setNewPassword(mobileno,newPassword,confirmPassword,role){

  const data = new FormData();
     data.append("mobile_no",mobileno);
     data.append("new_password",newPassword);
     data.append("confirm_password",confirmPassword);
     data.append("role",role);

     console.log(data);
     var datab={
     	'mobile_no':mobileno,
     	'new_password':newPassword,
     	'confirm_password':confirmPassword,
     	'role':role,

     }
     console.log(datab);
	// let data=JSON.stringify({mobile_no:mobileno,new_password:newPassword,confirm_password:confirmPassword,role:"dealer"});

 	return new Promise((resolve,reject) => {
    this.http.post("http://app.meltag.com/finolex/api/set-password",datab,{headers:this._header}).toPromise().then(data => {
    	console.log(data);
     	resolve(data.json());
    }, err => {
     	console.log(err);
     	reject(err);
    });
  });

	
 }

}
